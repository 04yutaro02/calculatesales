package jp.alhinc.sanuki_yutaro.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class CalculateSales {

	public static void main(String[] args){
		HashMap<String, String> definedMap = new HashMap<>();
		HashMap<String, Long> tallyMap = new HashMap<>();
		//コマンドライン引数の数が1つ以外だった場合終了
		 if (args.length != 1){
			 System.out.println("予期せぬエラーが発生しました");
			 return;
		}

		 //支店定義ファイルの読み込み
		 if(! readFile (args[0], "branch.lst", definedMap, tallyMap)){
			 return;
		 }

		//集計
		 if(! filterAdd (args[0], definedMap, tallyMap)){
			 return;
		 }

		//出力
		if(! writeFile (args[0], "branch.out", definedMap, tallyMap)){
			return;
		};

	}

	//支店定義ファイルの読み込み
	static boolean readFile (String path, String fileName, HashMap<String, String> defmap, HashMap<String, Long> talmap){
		File sourceFile = new File(path, fileName);
		if (!sourceFile.exists()){
			System.out.println("支店定義ファイルが存在しません");
			return false;
		}
		BufferedReader br = null;
		try {
			 //宣言
			FileReader fr = new FileReader(sourceFile);
			br = new BufferedReader (fr);
			String line;
			//定義ファイルの行がなくなるまでループ
			while((line = br.readLine()) != null){
				String [] defined = line.split(",", 0);
				//定義ファイルの行数が2行以外のとき終了
				if(defined.length != 2){
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return false;
				}
				//定義ファイルの支店名が数字三桁以外のとき終了
				if(!defined[0].matches("[0-9]{3}")){
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return false;
				}

				//マップへput
				defmap.put(defined[0], defined[1]);
				talmap.put(defined[0], 0L);
			}
		//エラー処理
		}catch(FileNotFoundException e){
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}catch(IOException e ){
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}finally{
			if ( br != null ){
				try{
					br.close();
				}catch(IOException e){
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}

	//支店売り上げファイルの選別と加算処理
	static boolean filterAdd (String path,  HashMap<String, String> map1, HashMap<String, Long> map2) {
		ArrayList<File> filterList = new ArrayList<File> ();
		File filePath = new File(path);
		File sort [] = filePath.listFiles();
		//Sortの中のファイルの数だけ読み込み、フォーマットにあったファイルだけをフィルターリストに追加
		for(int i=0; i<sort.length; i++){
			if(sort[i].isFile() && sort[i].getName().matches("^[0-9]{8}\\.rcd$" )){
					filterList.add(sort[i]);
			}
		}

		//宣言
		//フィルターリストの中身が連番じゃなかった時終了
		for(int i=0; i<filterList.size() - 1; i ++){
			int firstNum =  Integer.valueOf(filterList.get(i).getName().substring(1,8)).intValue();
			int secondNum =  Integer.valueOf(filterList.get(i + 1).getName().substring(1,8)).intValue();
			if ( firstNum != secondNum - 1){
				System.out.println("売上ファイル名が連番になっていません");
				return false;
			}
		}

		//リストの最後までループ
		for(int i=0; i<filterList.size(); i++){
			List<String> fileRow =  new ArrayList<String> ();
			BufferedReader br = null;
			try{
				FileReader fr = new FileReader(filterList.get(i));
				br = new BufferedReader (fr);
				String line;
				//行がなくなるまでループ
				while((line = br.readLine()) != null){
					fileRow.add(line);
				}

				//中身が2行以外なら終了
				if(fileRow.size() != 2){
					System.out.println(filterList.get(i).getName() + "のフォーマットが不正です");
					return false;
				}

				//支店名が定義ファイルに存在しない時終了
				if (!map1.containsKey(fileRow.get(0))){
					System.out.println(filterList.get(i).getName() +"の支店コードが不正です");
					return false;
				}

				//中身に数字以外が入っていた場合終了
				if(!fileRow.get(1).matches("^[0-9]*$")){
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}

				//宣言
				//今までの合計金額と今出てきた金額を加算
				Long sum = map2.get(fileRow.get(0)) + Long.parseLong(fileRow.get(1));
				if( sum.toString().length() > 10){
					//10桁以上なら終了
					System.out.println("合計金額が10桁を超えました");
					return false;
				}

				map2.put(fileRow.get(0),sum);
			//エラー処理
			}catch(FileNotFoundException e){
				System.out.println("予期せぬエラーが発生しました");
				return false;
			}catch(IOException e ){
				System.out.println("予期せぬエラーが発生しました");
				return false;
			}finally{
				if ( br != null ){
					try{
						br.close();
					}catch(IOException e){
						System.out.println("予期せぬエラーが発生しました");
						return false;
					}
				}
			}
		}
		return true ;
	}

	//ファイルの作成と書き込み
	static boolean writeFile (String path, String fileName, HashMap<String, String> map1, HashMap<String, Long> map2){
		//宣言
		BufferedWriter bw = null;
		File saveFile = new File (path, fileName);
		try{
			saveFile.createNewFile();
			FileWriter fr = new FileWriter (saveFile);
			bw = new BufferedWriter (fr);
			String sep = System.getProperty("line.separator");
			for(Map.Entry<String, String> entry : map1.entrySet()){
				bw.write(entry.getKey() + "," + entry.getValue() + ","  + map2.get(entry.getKey()) + sep);
			}
		//エラー処理
		}catch(IOException e ){
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}finally{
			if ( bw != null ){
				try{
					bw.close();
				}catch(IOException e){
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}
}







